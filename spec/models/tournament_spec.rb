# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Tournament do
  describe 'Tournament' do
    before do
      @tournament = Tournament.new(name: 'test', max_players: 20, prize_fund: 1000, end_registration_date: Time.current.tomorrow, players_per_group: 5)
      20.times do |i|
        player = User.create!(email: "user#{i}@gmail.com", password: "password#{i}")
        @tournament.players << player
      end
      @tournament.save!
    end

    it 'generated groups' do
      expect(@tournament.groups.size).to eq(4)
      expect(@tournament.groups.first.players.size).to eq(5)
      expect(@tournament.groups.last.players.size).to eq(5)
    end

    it 'generated matches' do
      expect(@tournament.groups.first.matches.size).to eq(20)
    end
  end
end
