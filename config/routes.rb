Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  mount Thredded::Engine => '/forum'

  devise_for :users
  root to: 'pages#index'

  resources :tournaments, only: [:index, :show] do
    put 'register_player'
  end

  resources :streams, only: [:index]
  resources :articles, only: [:index, :show]

  namespace :dashboard do
    resources :tournaments, only: [:index, :show]
    resources :messages, only: [:create]
    resources :submissions, only: [:create] do
      put 'confirm'
      put 'decline'
    end

    get 'submit/:match_id', to: 'submissions#submit', as: :submit_match
    get 'messages/new/:nickname', to: 'messages#new', as: :send_message
  end

  get 'profile/:slug', to: 'profiles#show', as: :profile
  get 'messages', to: 'thredded/private_topics#index', as: :messages

  get 'contacts', to: 'pages#contacts'
end
