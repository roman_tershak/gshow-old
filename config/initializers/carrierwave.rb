CarrierWave.configure do |config|
  config.fog_provider = 'fog/aws'
  config.cache_dir = "#{Rails.env}/tmp/uploads"

  # config.storage = :file
  # config.root = "public/"
  # config.enable_processing = true

  # fog
  config.fog_credentials = {
    provider:              'AWS',
    aws_access_key_id:     ENV['S3_KEY'],
    aws_secret_access_key: ENV['S3_SECRET'],
    region:                ENV['S3_REGION']
    # host:                  ENV['S3_ASSET_URL']
    # endpoint:              'https://s3.example.com:8080' # optional, defaults to nil
  }
  config.storage = :fog
  config.fog_public = true
  config.fog_directory = ENV['S3_BUCKET_NAME']

  config.asset_host = ENV['S3_ASSET_URL']
  config.fog_attributes = {
      'Cache-Control' => 'public, max-age=604800',
      'Expires'       => "#{168.hours.from_now.httpdate}"
  }
end
