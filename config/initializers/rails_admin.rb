RailsAdmin.config do |config|

  ### Popular gems integration

  # == Devise ==
  config.authenticate_with do
    warden.authenticate! scope: :user
  end
  config.current_user_method(&:current_user)

  # == Cancan ==
  config.authorize_with :cancancan

  config.parent_controller = 'ApplicationController'
  ## == Pundit ==
  # config.authorize_with :pundit

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  ## == Gravatar integration ==
  ## To disable Gravatar integration in Navigation Bar set to false
  # config.show_gravatar = true

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    bulk_delete
    show
    edit
    delete
  end

  config.model 'User' do
    label 'Пользователи'

    list do
      field :nickname
      field :email
      field :role
    end
  end

  config.model 'Tournament' do
    label 'Турниры'

    list do
      field :name
      field :max_players
      field :end_registration_date
      field :prize_fund
      field :password, :string
    end

    edit do
      include_all_fields
      field :rules, :froala
      field :password, :string
    end
  end

  config.model 'Group' do
    label 'Группы'

    list do
      field :tournament
      field :matches
    end
  end

  config.model 'Match' do
    label 'Матчи'

    list do
      field :players
      field :winner
      field :submissions
    end

    edit do
      field :players
      field :winner
      field :submissions
    end
  end

  config.model 'Stream' do
    label 'Стримы игроков'

    list do
      scopes [:present]

      field :user
      field :link
      field :status
    end
  end

  config.model 'Article' do
    label 'Новости'

    edit do
      include_all_fields
      field :body, :froala
    end
  end

  config.model 'Submission' do
    label 'Результаты матчей'
  end

  config.included_models = [User, Tournament, Group, Match, Submission, Stream, Article]
end
