class AddRulesForTournaments < ActiveRecord::Migration[5.2]
  def change
    add_column :tournaments, :rules, :string
  end
end
