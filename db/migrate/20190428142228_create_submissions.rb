class CreateSubmissions < ActiveRecord::Migration[5.2]
  def change
    create_table :submissions do |t|
      t.integer :match_id
      t.boolean :win
      t.integer :user_id
      t.integer :status, default: 0
    end
  end
end
