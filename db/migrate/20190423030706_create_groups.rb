class CreateGroups < ActiveRecord::Migration[5.2]
  def change
    create_table :groups do |t|
      t.string :label
      t.integer :tournament_id
    end

    create_table :matches do |t|
      t.integer :winner_id
    end
  end
end
