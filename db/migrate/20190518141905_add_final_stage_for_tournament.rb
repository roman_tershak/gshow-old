class AddFinalStageForTournament < ActiveRecord::Migration[5.2]
  def change
  	add_column :tournaments, :next_stage_length, :integer, default: 8
  end
end
