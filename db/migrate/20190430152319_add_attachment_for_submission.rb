class AddAttachmentForSubmission < ActiveRecord::Migration[5.2]
  def change
    add_column :submissions, :proof, :string
  end
end
