class AddPasswordFieldForTournament < ActiveRecord::Migration[5.2]
  def change
    add_column :tournaments, :password, :string
  end
end
