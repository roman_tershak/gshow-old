class AddFieldsForTournament < ActiveRecord::Migration[5.2]
  def change
    add_column :tournaments, :players_per_group, :integer, default: 8
  end
end
