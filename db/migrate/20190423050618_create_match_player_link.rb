class CreateMatchPlayerLink < ActiveRecord::Migration[5.2]
  def change
    create_table :matches_users, id: false do |t|
      t.belongs_to :user, index: true
      t.belongs_to :match, index: true
    end
  end
end
