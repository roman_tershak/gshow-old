(0..5).each do |i|
  Tournament.create(name: "Tournament title #{i}",
                    max_players: 32,
                    end_registration_date: Time.current.yesterday,
                    prize_fund: (i+1)*100
  )
end

(6..20).each do |i|
  Tournament.create(name: "Tournament title #{i}",
                    max_players: 32,
                    end_registration_date: Time.current.tomorrow,
                    prize_fund: (i+1)*100
  )
end
