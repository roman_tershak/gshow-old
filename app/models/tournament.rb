class Tournament < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: :slugged

  has_and_belongs_to_many :players, class_name: 'User'
  has_many :groups

  mount_uploader :avatar, AvatarUploader

  scope :sorted_by_priority, -> { all.sort {|a, b| a.status_index <=> b.status_index } }
  after_save :generate_groups, if: -> { groups.empty? && max_players == players.count }

  STATUSES = ['Live', 'Scheduled', 'Finished']

  def status
    return 'Finished' if winner

    if max_players == players.count || Time.current > end_registration_date
      'Live'
    else
      'Scheduled'
    end
  end

  def category
    'Poker'
  end

  def status_index
    STATUSES.index(status)
  end

  def winner
    false
  end

  def generate_groups
    tournament_players = self.players.shuffle

    group_count = tournament_players.count / self.players_per_group
    label = 'A'

    (0...group_count).each do |i|
      group = self.groups.create!(label: label)

      tournament_players[(i * players_per_group)...(i * players_per_group + players_per_group)].each do |player|
        group.players << player
      end

      group.save
      label = label.next
    end
  end
end
