class Article < ApplicationRecord
  extend FriendlyId
  friendly_id :title, use: :slugged

  mount_uploader :image, AvatarUploader

  default_scope { order(updated_at: :desc) }

  enum status: [:draft, :published]

  scope :published, -> { where(status: :published) }
end
