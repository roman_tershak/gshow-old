class User < ApplicationRecord
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  enum role: [:player, :admin]

  has_and_belongs_to_many :tournaments
  has_and_belongs_to_many :groups
  has_and_belongs_to_many :matches
  has_many :submissions
  has_one :stream

  accepts_nested_attributes_for :stream

  validates :nickname, presence: true, uniqueness: { case_sensitive: false }, length: { in: 3..15 }
  validates_format_of :nickname, with: /^[a-zA-Z0-9_\.]*$/, multiline: true
  validates :email, presence: true
  validates :role, presence: true

  after_create :generate_stream

  def title
    nickname
  end

  def wins_count
    matches.where(winner_id: id).count
  end

  def lose_count
    matches.where.not(winner_id: id).count
  end

  def admin
    admin?
  end

  def moderator
    false
  end

  class << self
    def find_by_slug!(slug)
      User.find_by!(nickname: slug)
    end
  end

  private

  def generate_stream
    Stream.create!(user: self)
  end
end
