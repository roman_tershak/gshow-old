class Match < ApplicationRecord
  belongs_to :group
  has_and_belongs_to_many :players, class_name: 'User'
  has_many :submissions

  scope :scheduled_matches, -> { includes(:submissions).reject { |match| match.submissions.any? } }
  scope :sended_matches, -> { includes(:submissions).where({ submissions: { status: :sended }}) }
  scope :declined_matches, -> { includes(:submissions).where({ submissions: { status: :declined }}) }
  scope :submitted_matches, -> { includes(:submissions).where({ submissions: { status: :submitted }}) }

  def players_include?(user_id)
    players.pluck(:id).include?(user_id)
  end

  def sended?
    submissions.where(status: :sended).any?
  end

  def versus(user_id)
    vs = players.pluck(:id)
    vs.delete(user_id)
    vs.first
  end

  def versus_user(user_id)
    User.find(versus(user_id))
  end

  def winner
    return '' unless winner_id

    User.find(winner_id).nickname
  end
end
