class Group < ApplicationRecord
  has_many :matches
  has_and_belongs_to_many :players, class_name: 'User'
  belongs_to :tournament

  after_save :generate_matches, if: -> { matches.empty? }

  def all_matches
    matches.scheduled_matches +
    matches.sended_matches +
    matches.declined_matches +
    matches.submitted_matches
  end

  def sended_matches(id)
    matches.sended_matches.reject {|match| !match.players_include?(id) || match.submissions.last.user.id == id }
  end

  def sorted_players
    players.sort {|a, b| matches.submitted_matches.where(winner_id: b.id).size <=> matches.submitted_matches.where(winner_id: a.id).size }
  end

  private

  def generate_matches
    self.players.each do |player_one|
      self.players.each do |player_two|
        pairs = []

        self.matches.find_each do |match|
          pairs << match.players.pluck(:id)
        end

        next if player_one == player_two || pairs.include?([player_two.id, player_one.id])

        match = self.matches.create!

        match.players << player_one
        match.players << player_two
      end
    end
  end
end
