class Submission < ApplicationRecord
  belongs_to :match
  belongs_to :user

  enum status: [:sended, :declined, :submitted]

  mount_uploader :proof, AvatarUploader

  after_update :set_winner, if: -> { status == 'submitted' }

  private

  def set_winner
    if win
      match.update!(winner_id: user.id)
    else
      match.update!(winner_id: match.versus(user.id))
    end
  end
end
