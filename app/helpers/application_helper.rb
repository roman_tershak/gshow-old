module ApplicationHelper
  def dashboard_path?
    request.path =~ /dashboard/
  end

  def messages_path?
    request.path =~ /messages/ || request.path =~ /private-topics/
  end

  def forum_path?
    request.path =~ /forum/
  end

  def articles_path?
    request.path =~ /articles/
  end
end
