class StreamsController < ApplicationController
  def index
    @streams = Stream.online
  end
end
