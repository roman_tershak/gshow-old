class PagesController < ApplicationController
  def index
    @article = Article.published.first
  end

  def contacts
  end
end
