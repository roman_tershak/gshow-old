module Dashboard
  class MessagesController < ApplicationController
    before_action :authenticate_user!

    def new
      @oponent_nickname = params[:nickname]

      respond_to do |format|
        format.html
        format.js
      end
    end

    def create
      controller = Thredded::PrivateTopicsController.new
      controller.request = request
      controller.response = response
      controller.create

      redirect_to dashboard_tournament_path(current_user.tournaments.last), notice: 'Сообщения успешно отправлено!'
    end
  end
end
