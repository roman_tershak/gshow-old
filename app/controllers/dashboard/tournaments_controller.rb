module Dashboard
  require 'will_paginate/array'

  class TournamentsController < ApplicationController
    before_action :authenticate_user!

    def index
      @tournaments = current_user.tournaments.sorted_by_priority.paginate(page: params[:page], per_page: 12)
    end

    def show
      @tournament = current_user.tournaments.friendly.find(params[:id])

      label = 'A'
      @tournament.groups.includes(:players).each do |group|
        label = group.label if group.players.pluck(:id).include?(current_user.id)
      end

      if @tournament.groups.any?
        @group = @tournament.groups.find_by!(label: params['group'] || label)
        @another_groups = @tournament.groups.where.not(label: @group&.label).pluck(:label)
      end
    end
  end
end
