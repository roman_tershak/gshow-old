module Dashboard
  class SubmissionsController < ApplicationController
    before_action :authenticate_user!
    before_action :check_permission, except: [:confirm, :decline]

    def submit
      respond_to do |format|
        format.html
        format.js
      end
    end

    def create
      @submission = Submission.create!(match_id: params[:submission][:match_id], win: ActiveModel::Type::Boolean.new.cast(params[:submission][:win]), proof: params[:submission][:proof], user_id: current_user.id)

      redirect_to dashboard_tournament_path(@submission.match.group.tournament), notice: 'Отправлено! Ожидается подтверждения противника.'
    end

    def confirm
      @submission = Submission.find(params[:submission_id])
      @submission.submitted!

      redirect_to dashboard_tournament_path(@submission.match.group.tournament), notice: 'Данные внесены в таблицу!'
    end

    def decline
      @submission = Submission.find(params[:submission_id])
      @submission.declined!

      redirect_to dashboard_tournament_path(@submission.match.group.tournament), alert: 'Результат отклонен!'
    end

    private

    def check_permission
      @match = Match.find(params[:match_id] || params[:submission][:match_id])

      raise CanCan::AccessDenied unless @match.players_include?(current_user.id)
    end
  end
end
